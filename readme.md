The Bionic Harp
===============

**A CIRMMT Student Award Project for 2019/2020**

John Sullivan  
Alex Tibbitts

details forthcoming...

*See [work_sessions](./work_sessions) folder for notes (1 doc per session)*

----

### Basic project overview and schedule

![whiteboard1_overview](work_sessions/media/2019-09-11_whiteboard1.jpg)

### Interface design ideas

![whiteboard2_interface_ideas](work_sessions/media/2019-09-11_whiteboard2.jpg)

----

